# CCDC Attack Kit

```
$ nmap -oG results -p22 192.168.1.0/24
$ ./gen.sh                            
Usage: gen.sh <attacker_ip> <port> <fake_ip> <nmap_results>
$ 
$ ./gen.sh 192.168.1.50 443 8.8.8.8 results
Targeting 47 hosts. Ready (y/n)? y
... <snip> ...
```



### How to use
Use gen.sh to generate payloads for linux machines. At the end of the script, fabric is executed uploading the payloads and running a few commands on each machine.
A reverse shell and a daemon script is placed on each machine called the red team daemon (crd.pl). This daemon checks on the other machines locally in the network
and reinfects with default credentials if a machine is scrubbed. 

The daemon will use the tools and payload in winattack and linattack folders to reinfect.
Each linux machine has the capability to become a scrub checker with the touching of a particular file on the filesystem.